#launch the file
$ roslaunch pizzamaker_query pizza_maker.launch

# run the knowledgebase node 
$ rosrun pizzamaker_query knowledgebase

# call the service to enter the food type 
$ rosservice call /get_pizza_menu "Food: 'Pizza'"

# run the userinput node to get the message of ordered pizza
$ rosrun pizzamaker_query user_ip

# run the masternode to publish the message of pizzatype to knowledgebase
$ rosrun pizzamaker_query master_node

# call the service to get the ingredients data from knowledgebase
$ rosservice call /make_pizza "pizzatype: 'Margarita'"


