# SSY236_Åke the pizza maker_project 

This project is to fulfill SSY235 course requirements. Carried out by group 4

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Copy the world and map files to the correct folders and open the world

```
cp worlds/pizza_env.world ~/simulation_ws/src/tiago_simulation/tiago_gazebo/worlds/
cp -r maps/2022-01-06_173139 ~/simulation_ws/src/maps

roslaunch tiago_gazebo tiago_gazebo.launch public_sim:=true robot:=steel world:=pizza_env
```

## Run each commands in the separate terminal


```
# To take food type, display menu, consult ontology for ingredients, robotics part, tiago takes the id of the ingredients traverses to the location, grabs the ingredients(aruco) and brings it to the preparation table
$ roslaunch pizza_maker pizza_maker.launch
```

```
# Display pizza menu, take pizza order and advertise ingredients for further steps
$ rosrun pizzamaker_query masternode
```


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Supreeth-Ramachandra/ssy236_ake-the-pizza-maker_project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://gitlab.com/Supreeth-Ramachandra/ssy236_ake-the-pizza-maker_project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

##Use the built-in continuous integration in GitLab.

##- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://gitlab.com/-/experiment/new_project_readme_content:60c066c7d3f3e65abe78d45eb112db74?https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## License
This is open source. No permission needed to transmit

