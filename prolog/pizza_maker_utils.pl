:- module(pizza_maker_utils,
    [
	create_instance_from_class/3,
	getClassPath/2
    ]).

:- use_module(library('semweb/rdfs')).
:- use_module(library('semweb/rdf_db')).

% TODO: Change the path and variable.
:- rdf_db:rdf_register_ns(a4Ontology, 'http://www.pizza.org/pizzaontology/pizza_kb.owl#', [keep(true)]).

%%%%%%%%%%%%%% Custom computables %%%%%%%%%%%%%%%%%%%%%%

% This function will create an instance of a desired class
% create_instance_from_class(+Class, +Instance_ID, ?Instance)
% The created instance will have the predicate/property rdf:type
% to correctly inheritate the properties of its super-classes
%
% @param Class		represents the name of the class where the instance will be created.
%					Class could be of two forms:
%					Class='Margarita' 
% @param Instance_ID	is the new ID that the instance will have
% @param Instance	asserted new instance
create_instance_from_class(Class, Instance_ID, Instance) :-
	% Check ID and class path
	getClassPath(Class,Class_path),
	
	% Create the path of the new instance
	atom_concat(Class_path,  '_', Class2),
	atomic_concat(Class2, Instance_ID, Instance),
	% write(Instance),nl,
	% assert/create the new instance
	rdf_assert(Instance, rdf:type, Class_path).

% This function will return the path of the class/instance given
% getClassPath(+Class, ?Class_path)
%
% @param Class		represents the name of the class where the instance will be created.
%					Class could be of two forms:
%					Class='Margarita'  
% @param Class_path	correct class full path

getClassPath(Class, Class_path):-
	((concat_atom(List, '#', Class),length(List,Length),Length>1) ->
	( % Class has already a URI
	   Class_path=Class );
	  % When the class does not have URL, will get the knowrob path
        ( TempClass='http://www.pizza.org/pizzaontology/pizza_kb.owl#',
	atom_concat(TempClass, Class, Class_path)
	% write(Class_path), nl
 	)).
