:- register_ros_package(knowrob_maps).
:- register_ros_package(knowrob_actions).
:- register_ros_package(knowrob_common).


:- consult('pizza_maker_utils.pl').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% parse OWL files, register name spaces
:- owl_parser:owl_parse('package://pizza_maker/owl/pizza_kb.owl').
:- rdf_db:rdf_register_ns(a4Ontology, 'http://www.pizza.org/pizzaontology/pizza_kb.owl#', [keep(true)]).


