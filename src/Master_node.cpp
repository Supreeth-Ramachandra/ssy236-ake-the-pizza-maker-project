#include "pizza_maker/ingredientid.h"
#include "pizza_maker/kbtransfer.h"
#include "pizza_maker/userinput.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <cctype>
#include <iostream>
#include <pizza_maker/fetch_ingredient.h>
#include <string>

using namespace std;

void tokenize(std::string const &str, const char delim,
              std::vector<std::string> &out) {
  size_t start;
  size_t end = 0;

  while ((start = str.find_first_not_of(delim, end)) != std::string::npos) {
    end = str.find(delim, start);
    out.push_back(str.substr(start, end - start));
  }
}

bool get_pizza_menu(pizza_maker::userinput::Request &req,
                    pizza_maker::userinput::Response &res) {

  res.menu = "Menu";
  return true;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "main_server");
  ros::NodeHandle n;
  ros::ServiceServer service =
      n.advertiseService("get_pizza_menu", get_pizza_menu);
  ros::ServiceClient client =
      n.serviceClient<pizza_maker::kbtransfer>("get_pizza_menu_kb");
  // Create an instance of the srv request type
  pizza_maker::kbtransfer pizzasrv;

  pizzasrv.request.Foodtype = "Pizza";

  // get the pizza name user wishes to have
  std::string inputString;
  std_msgs::String msg;
  if (client.call(pizzasrv)) {
    std::cout << "Choose the Pizza from the menu:" << std::endl;
    std::cout << pizzasrv.response.menudetails.c_str() << std::endl;
    std::getline(std::cin, inputString);
    inputString[0] = toupper(inputString[0]);
    std::cout << "Your order is : " << inputString << std::endl;
  }

  ros::ServiceClient client1 =
      n.serviceClient<pizza_maker::ingredientid>("make_pizza");
  pizza_maker::ingredientid pizzatypesrv;
  pizzatypesrv.request.pizzatype = inputString.c_str();
  ros::ServiceClient client2 =
      n.serviceClient<pizza_maker::fetch_ingredient>("/fetch_ingredient");
  pizza_maker::fetch_ingredient ingredient_id;
  // get the pizza name user wishes to have
  if (client1.call(pizzatypesrv)) {
    std::cout << "Ingredients:" << std::endl;
    std::cout << pizzatypesrv.response.ingredients << std::endl;
    for (int i = 0; i < 4; i++) {
      if (pizzatypesrv.response.ingredients_ids[i] != 0) {
        ingredient_id.request.marker_ID =
            (long int)pizzatypesrv.response.ingredients_ids[i];
        ROS_INFO("ID: %ld", ingredient_id.request.marker_ID);
        client2.call(ingredient_id);
      }
    }
  }
  ros::spin();
  return 0;
}