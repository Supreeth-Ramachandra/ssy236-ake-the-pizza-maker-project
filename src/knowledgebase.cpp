// ingredientid.h header file generated from the ingredientid.srv file
#include "pizza_maker/ingredientid.h"
// kbtransfer.h header file generated from the kbtransfer.srv file
#include "pizza_maker/kbtransfer.h"
// userinput.h header file generated from the userinput.srv file
#include "pizza_maker/userinput.h"
#include "ros/ros.h"
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <rosprolog/rosprolog_client/PrologClient.h>
using namespace std;

// Function to tokenize the string
void tokenize(std::string const &str, const char delim,
              std::vector<std::string> &out) {
  size_t start;
  size_t end = 0;

  while ((start = str.find_first_not_of(delim, end)) != std::string::npos) {
    end = str.find(delim, start);
    out.push_back(str.substr(start, end - start));
  }
}

// call back function to obtain the ingredients from ontology and return id for
// each ingredient
bool make_pizza(pizza_maker::ingredientid::Request &req,
                pizza_maker::ingredientid::Response &res) {
  res.ingredients_ids.resize(5);
  // Assigning unique id for each ingredient
  map<int, string> ingredient_map = {{
                                         1,
                                         "Tomato",
                                     },
                                     {
                                         2,
                                         "Mozzarella",
                                     },
                                     {
                                         3,
                                         "Onion",
                                     },
                                     {
                                         4,
                                         "ChickenTopping",
                                     },
                                     {
                                         5,
                                         "salmon",
                                     },
                                     {
                                         6,
                                         "Jalapeno",
                                     },
                                     {
                                         7,
                                         "Corn",
                                     }};
  // query to obtain ingredients
  PrologClient pl = PrologClient("/rosprolog", true);
  // query example
  // owl_subclass_of('http://www.pizza.org/pizzaontology#Margarita',Restr),rdf(Restr,owl:onProperty,P),rdf(Restr,owl:someValuesFrom,D).
  string query_string =
      "owl_subclass_of('http://www.pizza.org/pizzaontology#" + (req.pizzatype) +
      "',Restr),rdf(Restr,owl:onProperty,'http://www.pizza.org/"
      "pizzaontology#hasTopping'),rdf(Restr,owl:someValuesFrom,D)";

  PrologQuery bdgs = pl.query(query_string);
  std::string ingredientsstring;
  int count = 0;
  for (PrologQuery::iterator it = bdgs.begin(); it != bdgs.end(); it++) {
    PrologBindings bdg = *it;
    std::string output = bdg["D"].toString();
    const char delim = '#';
    const char delim2 = ',';
    std::vector<std::string> out;
    tokenize(output, delim, out);

    for (auto &outstring : out) {
      if (outstring != "http://www.pizza.org/pizzaontology") {

        for (const auto &kv : ingredient_map) {
          // identify corresponding id for the ingredient and return the data to
          // master_node
          if (outstring == kv.second) {
            // return the id for each ingredient
            res.ingredients_ids[count] = kv.first;
            count = count + 1;
            ingredientsstring += kv.second + ",";
          }
        }
      }
    }
  }

  std::string final_string =
      ingredientsstring.substr(0, ingredientsstring.length() - 1);
  // return the  each ingredient
  res.ingredients = final_string.c_str();

  return true;
}

// call back function to the varieties of pizza from the ontology file
bool get_pizza_menu_kb(pizza_maker::kbtransfer::Request &req,
                       pizza_maker::kbtransfer::Response &res) {
  int count = 0;
  PrologClient pl = PrologClient("/rosprolog", true);
  // query to fetch the subclass of Pizza
  string query_string =
      "owl_subclass_of(A,'http://www.pizza.org/pizzaontology#Pizza')";

  PrologQuery bdgs = pl.query(query_string);
  std::string menustring;
  for (PrologQuery::iterator it = std::next(bdgs.begin()); it != bdgs.end();
       it++) {
    PrologBindings bdg = *it;
    std::string output = bdg["A"].toString();
    const char delim = '#';
    std::vector<std::string> out;

    tokenize(output, delim, out);

    for (auto &items : out) {
      if (items != "http://www.pizza.org/pizzaontology") {
        count = count + 1;
        // obtain the different types of pizza
        menustring += "\n" + std::to_string(count) + "." + items + "\n";
      }
    }
  }
  res.menudetails = menustring;
  return true;
}

// function to handle the communication between ontology file
int main(int argc, char **argv) {
  ros::init(argc, argv, "knowledgebase_server");
  ros::NodeHandle n;
  ros::ServiceServer service =
      n.advertiseService("get_pizza_menu_kb", get_pizza_menu_kb);
  ROS_INFO("Pizza menu displayed");
  ros::ServiceServer service1 = n.advertiseService("make_pizza", make_pizza);
  ros::spin();
  return 0;
}