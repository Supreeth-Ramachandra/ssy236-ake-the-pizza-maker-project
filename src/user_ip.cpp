// userinput.h header file generated from the userinput.srv file
#include "pizza_maker/userinput.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

// Node to get the user input
int main(int argc, char **argv) {

  // Initialize the node and set the name to menu_userinput
  ros::init(argc, argv, "menu_userinput");

  ros::NodeHandle n;

  // Create a client for the service named get_pizza_menu
  ros::ServiceClient client =
      n.serviceClient<pizza_maker::userinput>("get_pizza_menu");

  // Create an instance of the pizzasrv request type to request and obtain
  // response from masternode
  pizza_maker::userinput pizzasrv;

  pizzasrv.request.Food = "Pizza";
  ros::spin();
  return 0;
}