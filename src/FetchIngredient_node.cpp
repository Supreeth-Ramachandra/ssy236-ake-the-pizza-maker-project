#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <aruco_msgs/MarkerArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <pizza_maker/fetch_ingredient.h>
#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_srvs/Empty.h>
#include <trajectory_msgs/JointTrajectory.h>

std_msgs::Int32 marker_ID;

trajectory_msgs::JointTrajectory compose_torso_msg() {

  trajectory_msgs::JointTrajectory torso_msg;
  torso_msg.joint_names.push_back("torso_lift_joint");
  trajectory_msgs::JointTrajectoryPoint pts;
  pts.positions.push_back(2);
  pts.time_from_start.sec = 3;
  pts.time_from_start.nsec = 0;
  torso_msg.points.push_back(pts);
  return torso_msg;
}

trajectory_msgs::JointTrajectory compose_head_msg() {

  trajectory_msgs::JointTrajectory head_msg;
  head_msg.joint_names.push_back("head_1_joint");
  head_msg.joint_names.push_back("head_2_joint");
  trajectory_msgs::JointTrajectoryPoint pts;
  pts.positions.push_back(0);
  pts.positions.push_back(-0.8);
  pts.time_from_start.sec = 3;
  pts.time_from_start.nsec = 0;
  head_msg.points.push_back(pts);
  return head_msg;
}

move_base_msgs::MoveBaseGoal get_table_pos(int table_nr) {
  move_base_msgs::MoveBaseGoal goal;
  switch (table_nr) {
  case 1:
    goal.target_pose.header.stamp = ros::Time::now();
    //   reference frame is map
    goal.target_pose.header.frame_id = "map";
    //   set goal as 2D coordinate
    goal.target_pose.pose.position.x = 3.6;
    goal.target_pose.pose.position.y = 2;
    //   set goal orientation
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = 0.2;
    goal.target_pose.pose.orientation.w = 1;
    break;
  case 2:
    goal.target_pose.header.stamp = ros::Time::now();
    //   reference frame is map
    goal.target_pose.header.frame_id = "map";
    //   set goal as 2D coordinate
    goal.target_pose.pose.position.x = 3.6;
    goal.target_pose.pose.position.y = -1;
    //   set goal orientation
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = 0.0;
    goal.target_pose.pose.orientation.w = 1.3;
    break;
  case 3: // Table 3 not yet implemented
    goal.target_pose.header.stamp = ros::Time::now();
    //   reference frame is map
    goal.target_pose.header.frame_id = "map";
    //   set goal as 2D coordinate
    goal.target_pose.pose.position.x = 1.68;
    goal.target_pose.pose.position.y = -2.6;
    //   set goal orientation
    goal.target_pose.pose.orientation.x = 0.0;
    goal.target_pose.pose.orientation.y = 0.0;
    goal.target_pose.pose.orientation.z = -0.705;
    goal.target_pose.pose.orientation.w = 0.705;
    break;
  }
  return goal;
}

bool fetch_ingredient(pizza_maker::fetch_ingredient::Request &req,
                      pizza_maker::fetch_ingredient::Response &res) {
  // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> ac("move_base",
                                                                   true);

  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  ac.waitForServer(); // will wait for infinite time
  ROS_INFO("Action server started, sending goal.");
  move_base_msgs::MoveBaseGoal goal;

  ros::NodeHandle nodeHandle;
  //   ros::Publisher torso_pub =
  //       nodeHandle.advertise<trajectory_msgs::JointTrajectory>(
  //           "/torso_controller/command", 1000);
  //   ros::Publisher head_pub =
  //       nodeHandle.advertise<trajectory_msgs::JointTrajectory>(
  //           "/head_controller/command", 1000);
  ros::ServiceClient client =
      nodeHandle.serviceClient<std_srvs::Empty>("/pick_gui");
  ros::Publisher ID_pub =
      nodeHandle.advertise<std_msgs::Int32>("/marker_ID", 1000);

  int table_nr;
  marker_ID.data = req.marker_ID;
  ROS_INFO("Sending the marker_ID to DetectAruco. %i", marker_ID.data);
  ID_pub.publish(marker_ID);

  if (marker_ID.data < 4) {
    table_nr = 1;
  } else {
    table_nr = 2;
  }

  // Go to the corresponding table to pick up ingredient
  goal = get_table_pos(table_nr);
  ac.sendGoal(goal);
  ROS_INFO("Moving to table nr: %i", table_nr);
  // wait for the action to return
  bool finished_before_timeout = ac.waitForResult(ros::Duration(200.0));
  ROS_INFO("Finished goal?");

  if (finished_before_timeout) {
    actionlib::SimpleClientGoalState state = ac.getState();
    ROS_INFO("Action finished: %s", state.toString().c_str());
  } else
    ROS_INFO("Action did not finish before the time out.");

  // TODO: Raise torso and look down to see markers
  trajectory_msgs::JointTrajectory torso_msg = compose_torso_msg();
  trajectory_msgs::JointTrajectory head_msg = compose_head_msg();

  ROS_INFO("Preparing body to look for Aruco markers.");

  //   torso_pub.publish(torso_msg);
  //   head_pub.publish(head_msg);
  //   sleep(20);
  std_srvs::Empty empty_msg;
  client.call(empty_msg);

  // TODO: Call function to search for marker_ID and grasp the aruco_marker
  ROS_INFO("Wait for Tiago to grasp the aruco marker, then type "
           "something in the terminal to proceed.");

  // Go to table 3 to drop off ingredient
  table_nr = 3;
  goal = get_table_pos(table_nr);
  ac.sendGoal(goal);
  ROS_INFO("Moving to table nr: %i", table_nr);
  // wait for the action to return
  finished_before_timeout = ac.waitForResult(ros::Duration(200.0));

  if (finished_before_timeout) {
    actionlib::SimpleClientGoalState state = ac.getState();
    ROS_INFO("Action finished: %s", state.toString().c_str());
  } else
    ROS_INFO("Action did not finish before the time out.");

  // TODO: Call function to look for available space on the table and place the
  // aruco_marker

  res.success = true;
  return true;
}

int main(int argc, char **argv) {

  ros::init(argc, argv, "FetchIngredient_node");

  ros::NodeHandle n;

  ros::ServiceServer service =
      n.advertiseService("/fetch_ingredient", fetch_ingredient);

  ROS_INFO("Ready to fetch ingredients.");

  while (ros::ok()) {

    ros::spin();
  }

  return 0;
}