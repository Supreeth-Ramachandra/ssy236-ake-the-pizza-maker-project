#include <aruco_msgs/MarkerArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <ros/ros.h>
#include <std_msgs/Int32.h>

int marker_ID = 0;

void get_aruco_poses(const aruco_msgs::MarkerArray &poses) {
  for (int i = 0; i < 9; i++) {
    if (poses.markers[i].id == marker_ID) {
      ROS_INFO("Marker found: %i", poses.markers[i].id);
      aruco_msgs::Marker aruco_pose = poses.markers[i];

      ros::NodeHandle nodeHandle;
      ros::Publisher pub =
          nodeHandle.advertise<aruco_msgs::Marker>("/aruco_single/pose", 1000);

      geometry_msgs::PoseStamped aruco_pose_msg;
      aruco_pose_msg.header = aruco_pose.header;
      aruco_pose_msg.pose = aruco_pose.pose.pose;

      ROS_INFO("Publishing single aruco marker.");
      pub.publish(aruco_pose_msg);
      ros::spinOnce();
    }
  }
}

void get_marker_ID(const std_msgs::Int32 marker_ID_msg) {
  marker_ID = marker_ID_msg.data;
  ROS_INFO("Marker_ID to look for: %i", marker_ID);
}

int main(int argc, char **argv) {

  ros::init(argc, argv, "DetectAruco_node");

  ros::NodeHandle n;

  ros::Subscriber pose_sub =
      n.subscribe("/aruco_marker_publisher/markers", 1000, get_aruco_poses);

  ros::Subscriber ID_sub = n.subscribe("/marker_ID", 1000, get_marker_ID);
  ROS_INFO("Ready to detect aruco marker");

  while (ros::ok()) {

    ros::spin();
  }

  return 0;
}